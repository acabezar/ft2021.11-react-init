import './App.css';
import Button from './components/Button';
import HelloProps from './components/HelloProps/HelloProps';
import ButtonProps from './components/ButtonProps/ButtonProps';
import Card from './components/Card/Card';
import ButtonClick from './components/ButtonClick/ButtonClick';
import Counter from './components/Counter/Counter';
import BasicForm from './components/BasicForm/BasicForm';
import HookForm from './components/HookForm/HookForm';
import ClickEmitter from './components/ClickEmitter/ClickEmitter';
import List from './components/List/List';
import TeaList from './components/TeaList/TeaList';
import Student from './components/Student/Student';
import Accordion from './components/Accordion/Accordion';

const nameClass = "red";
const dataCard = {
  imgUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Snow.jpg/640px-Snow.jpg",
  title: "Soy un titulo",
  description: "Soy una descripción"
}

const dataCard2 = {
  imgUrl: "https://imgcy.trivago.com/c_limit,d_dummy.jpeg,f_auto,h_1300,q_auto,w_2000/itemimages/16/07/160773_v8.jpeg",
  title: "Soy un titulo",
  description: "Soy una descripción"
}


const teas = [
  {name: "Te amarillo", imgUrl: "https://uploads-ssl.webflow.com/5ba516c6e46073724618dee5/5e3a7f3ae0b913bef57eda0f_las%20propiedades%20del%20t%C3%A9%20amarillo.jpg" },
  {name: "Te verde", imgUrl: "https://s1.eestatic.com/2015/03/12/cocinillas/cocinillas_17508326_115880897_1706x960.jpg" },
  {name: "Te blanco", imgUrl: "https://www.lavanguardia.com/files/og_thumbnail/files/fp/uploads/2021/04/28/6089293a5021a.r_d.4285-2191-0.jpeg" },
  {name: "Te negro", imgUrl: "https://t2.uc.ltmcdn.com/images/5/0/3/img_cuales_son_las_contraindicaciones_del_te_negro_41305_orig.jpg" },
  {name: "Te azul", imgUrl: "https://s3.amazonaws.com/arc-wordpress-client-uploads/infobae-wp/wp-content/uploads/2018/02/06175921/te-oolong-2.jpg" },
]

const infoStudent = {
  name: "Luisa Fernanda de los Astros modernos",
  score: 8.5
}

const infoStudent2 = {
  name: "Manuela de los infantes no tan infantes",
  score: 3.5
}

function App() {

  const darDeComer = (food) =>{
    console.log("doy de comer " + food)
  }
  
  return (
    <div className="App-header">
      <Button />
      {/* <Button></Button> */}
      <HelloProps className={nameClass} name="Abel" surname="Cabeza Román" />

      <ButtonProps text="Enviar" />
      <ButtonProps text="Cancelar" />
      <ButtonProps text="Eliminar" />

      <Card data={dataCard}/>
      <Card data={dataCard2}/>

      <ButtonClick/>
      <Counter/> 

      {/* Form */}
      
      <BasicForm/>

      <HookForm/>

      {/* Ejecutar funcion padre */}

      <ClickEmitter fnDarDeComer={darDeComer}/>


      {/* For */}

      <List/>

      <TeaList teas={teas}/>

      {/* If */}

      <Student info={infoStudent}/>
      <Student info={infoStudent2}/>

      <Accordion>
        <h1>Hey! me muestro!</h1>
      </Accordion>


    </div>
  );
}

export default App;
