import { useState } from "react";


const INITIAL_STATE = {
    name: '',
    address: '',
    location: '',
    image: '',
};

export default function BasicForm() {
    const [state, setState] = useState(INITIAL_STATE);


    const submitForm = (ev) => {
        ev.preventDefault();

        console.log(state)
    };

    const handleInput = (ev) => {
        setState({ ...state, [ev.target.name]: ev.target.value });
    }

    return <form onSubmit={submitForm}>
        <fieldset>
            <label>
                <p>Nombre</p>
                <input type="text" name="name" value={state.name} onChange={handleInput} />
            </label>

            <label>
                <p>Address</p>
                <input type="text" name="address" value={state.name} onChange={handleInput} />
            </label>
        </fieldset>

        <button>Enviar</button>
        {/* <input type="submit" value="Enviar" /> */}
    </form>
}