import { useState } from "react"

export default function Accordion({ children }) {
    const [isShowing, setIsShowing] = useState(false);

    return <div>
        <button onClick={() => setIsShowing(!isShowing)}>Change</button>
        {isShowing && children}
    </div>
}