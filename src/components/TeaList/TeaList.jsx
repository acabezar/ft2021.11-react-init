export default function TeaList({teas}){
    return <div>
        {teas.map(tea => 
            <figure key={tea.name}>
                <img src={tea.imgUrl} alt={tea.name} />
                <figcaption>{tea.name}</figcaption>
            </figure>
        ) }
    </div>
}