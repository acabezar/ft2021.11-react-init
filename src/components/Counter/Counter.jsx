import { useState } from "react";

export default function Counter(){
    const [counter, setCounter] = useState(0);

    return (<div>
        {counter}

        <button onClick={() => {
            setCounter(counter + 1)
            console.log(counter);
            }}>+</button>
        <button onClick={() => {setCounter(counter - 1)}}>-</button>
    </div>)
}