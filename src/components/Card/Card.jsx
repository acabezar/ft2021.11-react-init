import ButtonProps from "../ButtonProps/ButtonProps"
export default function Card({data}){
    return <div>
        <img src={data.imgUrl} alt={data.title} />
        <h3>{data.title}</h3>
        <p>{data.description}</p>
        <ButtonProps text="Ver detalle"/>
    </div>
}