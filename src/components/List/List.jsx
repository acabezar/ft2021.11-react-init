export default function List (){

    const list = ["Mercurio", "Venus", "Tierra", "Marte"];

    // const renderList = () => {
    //     const listToRender = []
    //     for (let index = 0; index < list.length; index++) {
    //         listToRender.push(<li key={index}>{list[index]}</li>)
    //     }

    //     return listToRender;
    // }

    return <ul>
        {/* {renderList()} */}
        {list.map((item, index) => <li key={index}>{item}</li>)}
    </ul>
}