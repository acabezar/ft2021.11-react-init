export default function HelloProps({className, name, surname}) {

    return <h1 className={className}>Hello {name} {surname}</h1>
}