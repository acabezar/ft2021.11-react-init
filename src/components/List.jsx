import Button from "./Button";

export default function List(){
    return (
        <>
            <ul>
                <li>Fresa</li>
                <li>Pomelo</li>
                <li><Button></Button></li>
                <li>Melón</li>
            </ul>

            <ul>
                <li>Lujuria</li>
                <li>Gula</li>
                <li>Pereza</li>
            </ul>
        </>
    )
}
